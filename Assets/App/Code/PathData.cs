﻿using System.Collections.Generic;

namespace App.Code
{
    public class PathData
    {
        public ICluster Enter { get; set; }
        public ICluster Exit { get; set; }

        public List<ICluster> Path { get; set; }

        public PathData() =>
            Path = new List<ICluster>();
    }
}