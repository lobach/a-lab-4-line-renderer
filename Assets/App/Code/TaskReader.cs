﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace App.Code
{
    public static class TaskReader
    {
        public static TaskData Read(string fileName)
        {
            var lines = File.ReadAllLines(fileName);
            var points = new List<Vector2Double>();
            foreach (var line in lines)
            {
                var values = line
                    .Replace('\t', ' ')
                    .Split(' ')
                    .ToArray();
                points.Add(new Vector2Double(values[1].ToDouble(), values[2].ToDouble()));
            }

            return new TaskData
            {
                Points = points
            };
        }
    }
}