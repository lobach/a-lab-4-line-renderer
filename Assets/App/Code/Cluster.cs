﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace App.Code
{
    public class Cluster : ICluster
    {
        public Vector2Double Position
        {
            get
            {
                var sum = Vector2Double.Zero;
                foreach (var cluster in Clusters)
                    sum += cluster.Position;

                return sum / Clusters.Count;
            }
        }

        public int ClustersCount => Clusters.Count;
        public List<ICluster> Clusters { get; private set; }
        public PathData? SolveData { get; set; }

        private Guid _guid;

        public Cluster()
        {
            Clusters = new List<ICluster>();
            _guid = Guid.NewGuid();
        }

        public Cluster(List<ICluster> clusters)
        {
            _guid = Guid.NewGuid();
            Clusters = clusters;
        }

        public void Solve(ICluster? enter = null, ICluster? exit = null)
        {
            if (ClustersCount == 1)
            {
                SolveData = new PathData
                {
                    Enter = Clusters[0],
                    Exit = Clusters[0],
                    Path = new List<ICluster> {Clusters[0]}
                };
            }

            if (enter == null && exit == null)
            {
                var pd = new PathData
                {
                    Enter = Clusters[0],
                    Path = new List<ICluster> {Clusters[0]}
                };
                for (var i = 1; i < Clusters.Count; i++)
                {
                    var cluster = Clusters[i];
                    if (i == Clusters.Count - 1)
                    {
                        pd.Path.Add(cluster);
                        pd.Exit = cluster;
                        continue;
                    }

                    var minIndex = 0;
                    var minDistance = double.MaxValue;
                    for (var j = i + 1; j < Clusters.Count; j++)
                    {
                        var dist = Vector2Double.Distance(cluster.Position, Clusters[i].Position);
                        if (dist <= minDistance)
                        {
                            minDistance = dist;
                            minIndex = i;
                        }
                    }

                    pd.Path.Add(Clusters[minIndex]);
                }

                SolveData = pd;
                for (var i = 0; i < pd.Path.Count; i++)
                {
                    var cluster = pd.Path[i];
                    var prev = i == 0 ? pd.Path[^1] : pd.Path[i - 1];
                    var next = i == pd.Path.Count - 1 ? pd.Path[0] : pd.Path[i + 1];
                    var begin = cluster.ClosestTo(prev);
                    var end = cluster.ClosestTo(next, begin);
                    cluster.Solve(begin, end);
                }
            }
            else
            {
                var pd = new PathData
                {
                    Enter = enter,
                    Exit = exit,
                    Path = new List<ICluster> {enter}
                };

                var tmp = new List<ICluster>(Clusters);
                tmp.Remove(enter);
                if (enter is Point p)
                    tmp.Remove(p);

                tmp.Remove(exit);
                if (exit is Point p1)
                    tmp.Remove(p1);

                for (var i = 0; i < tmp.Count; i++)
                {
                    var cluster = pd.Path[i];
                    var minIndex = 0;
                    var minDistance = double.MaxValue;
                    for (var j = i; j < tmp.Count; j++)
                    {
                        var dist = Vector2Double.Distance(cluster.Position, tmp[i].Position);
                        if (dist <= minDistance)
                        {
                            minDistance = dist;
                            minIndex = i;
                        }
                    }

                    pd.Path.Add(Clusters[minIndex]);
                }

                pd.Path.Add(pd.Exit);
                SolveData = pd;
                for (var i = 0; i < pd.Path.Count; i++)
                {
                    var cluster = pd.Path[i];
                    var prev = i == 0 ? pd.Path[^1] : pd.Path[i - 1];
                    var next = i == pd.Path.Count - 1 ? pd.Path[0] : pd.Path[i + 1];
                    var begin = cluster.ClosestTo(prev);
                    var end = cluster.ClosestTo(next);
                    cluster.Solve(begin, end);
                }
            }
        }

        public void AddCluster(ICluster cluster)
        {
            Clusters.Add(cluster);
            //Position = (Position + cluster.Position) / 2;
        }

        public void Reduce()
        {
            if (ClustersCount <= App.MinClustersCount) return;

            var lst = new List<ICluster>(App.MinClustersCount);
            var tmp = new List<ICluster>(Clusters);
            var random = new Random();
            for (var i = 0; i < App.MinClustersCount; i++)
            {
                var item = random.Next(0, tmp.Count);
                lst.Add(new Cluster(new() {tmp[item]}));
                tmp.RemoveAt(item);
            }

            foreach (var cluster in tmp)
            {
                var minIndex = 0;
                var minDistance = double.MaxValue;
                for (var i = 0; i < lst.Count; i++)
                {
                    var dist = Vector2Double.Distance(cluster.Position, lst[i].Position);
                    if (dist <= minDistance)
                    {
                        minDistance = dist;
                        minIndex = i;
                    }
                }

                lst[minIndex].AddCluster(cluster);
            }

            foreach (var cluster in lst)
                cluster.Reduce();

            Clusters = lst;
        }

        public ICluster ClosestTo(ICluster cluster, ICluster? ignore = null)
        {
            if (Clusters.Count == 1)
                return Clusters[0];

            var tmp = new List<ICluster>(Clusters);
            if (ignore != null)
                tmp.Remove(ignore);

            var minIndex = 0;
            var minDistance = double.MaxValue;
            for (var i = 0; i < tmp.Count; i++)
            {
                var dist = Vector2Double.Distance(cluster.Position, tmp[i].Position);
                if (dist <= minDistance)
                {
                    minDistance = dist;
                    minIndex = i;
                }
            }

            return tmp[minIndex];
        }

        public double Distance()
        {
            var solve = Path();
            var dist = 0.0;
            for (var i = 0; i < solve.Count - 1; i++)
            {
                var cluster = solve[i];
                var next = solve[i + 1];
                dist += Vector2Double.Distance(cluster.Position, next.Position);
            }

            return dist;
        }

        public List<ICluster> Path()
        {
            if (Clusters.Count == 1)
                return new List<ICluster>() {this};

            var path = new List<ICluster>();
            foreach (var cluster in Clusters)
                path.AddRange(cluster.Path());

            return path;
        }

        public override string ToString() =>
            $"{_guid.ToString()}";
    }
}