﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace App.Code
{
    public class Launcher : MonoBehaviour
    {
        public DataFile[] Files;
        public LineRenderer LineRenderer;
        public GameObject PointPrefab;
        public Transform Parent;

        [Button(ButtonStyle.FoldoutButton)]
        private void ReadFiles()
        {
            var dir = new DirectoryInfo("./Data");
            var files = dir.GetFiles();
            Files = new DataFile[files.Length];
            for (var i = 0; i < files.Length; i++)
            {
                var file = files[i];
                Files[i] = new DataFile
                {
                    Launcher = this,
                    Path = file.FullName
                };
            }
        }

        [Button(ButtonStyle.FoldoutButton)]
        private void Run(string filePath, float scale)
        {
            var file = new FileInfo(filePath);
            Debug.Log($"FILE: {file.Name}");
            var taskData = TaskReader.Read(file.FullName);
            var app = new App(taskData);
            Debug.Log("STOCK");
            app.Run();
            Debug.Log("=======================");
            ShowPath(app.Path, scale);
        }

        [Button(ButtonStyle.FoldoutButton)]
        private void Clear()
        {
            LineRenderer.positionCount = 0;
            while (Parent.childCount > 0)
            {
                for (int i = 0; i < Parent.childCount; i++)
                    DestroyImmediate(Parent.GetChild(i).gameObject);
            }
        }

        private void ShowPath(List<ICluster> path, float scale)
        {
            Clear();
            var points = path.Select(c =>
                new Vector3((float) (c.Position.X * scale), 0.0f, (float) (c.Position.Y * scale))).ToArray();
            LineRenderer.positionCount = points.Length;
            LineRenderer.SetPositions(points);

            foreach (var point in points)
                Instantiate(PointPrefab, point, Quaternion.identity, Parent);
        }

        [Serializable]
        public class DataFile
        {
            public float Scale = 0.001f;
            public string Path;
            public Launcher Launcher;

            [Button(ButtonStyle.FoldoutButton)]
            private void Run() =>
                Launcher.Run(Path, Scale);
        }
    }
}