﻿using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace App.Code
{
    public class App
    {
        public static int MinClustersCount = 5;

        public int ClustersCount => Clusters.Count;
        public List<ICluster> Clusters { get; }
        public List<ICluster> Minimize { get; private set; }
        public List<ICluster> Path { get; private set; }

        private readonly TaskData _taskData;

        public App(TaskData taskData)
        {
            _taskData = taskData;
            Clusters = new List<ICluster>();
            foreach (var v in taskData.Points)
                Clusters.Add(new Point(v));
        }

        public void Run()
        {
            var sw = Stopwatch.StartNew();

            var cluster = new Cluster(Clusters);
            cluster.Reduce();
            cluster.Solve();
            var path = cluster.Path();
            var dist = 0.0;
            for (var i = 0; i < path.Count; i++)
            {
                var current = path[i];
                var next = i == path.Count - 1 ? path[0] : path[i + 1];
                dist += Vector2Double.Distance(current.Position, next.Position);
            }

            Path = path;
            UnityEngine.Debug.Log($"Distance: {dist}");
            sw.Stop();
            UnityEngine.Debug.Log($"Result:  in time: {sw.ElapsedMilliseconds} ms");
        }
    }
}