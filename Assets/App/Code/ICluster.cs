﻿using System.Collections.Generic;
using UnityEngine;

namespace App.Code
{
    public interface ICluster
    {
        public Vector2Double Position { get; }
        public PathData? SolveData { get; }
        public int ClustersCount { get; }
        public List<ICluster> Clusters { get; }
        public void Solve(ICluster? enter = null, ICluster? exit = null);
        public void AddCluster(ICluster cluster);
        public void Reduce();
        public ICluster ClosestTo(ICluster cluster, ICluster? ignore = null);
        public double Distance();
        public List<ICluster> Path();
    }
}