﻿using System;

namespace App.Code
{
    public struct Vector2Double
    {
        public double X { get; set; }
        public double Y { get; set; }
        public static Vector2Double Zero => new(0, 0);

        public Vector2Double(double x, double y)
        {
            X = x;
            Y = y;
        }

        public static double Distance(Vector2Double a, Vector2Double b) =>
            Math.Sqrt(Math.Pow(b.X - a.X, 2) + Math.Pow(b.Y - a.Y, 2));

        public static Vector2Double operator +(Vector2Double a, Vector2Double b) =>
            new(a.X + b.X, a.Y + b.Y);

        public static Vector2Double operator /(Vector2Double a, double b) =>
            new(a.X / b, a.Y / b);
    }
}