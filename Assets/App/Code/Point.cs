﻿using System.Collections.Generic;
using UnityEngine;

namespace App.Code
{
    public class Point : ICluster
    {
        public Vector2Double Position { get; }
        public PathData? SolveData { get; private set; }
        public int ClustersCount => 1;
        public List<ICluster> Clusters { get; }

        public Point(Vector2Double position)
        {
            Position = position;
            Clusters = new List<ICluster> {this};
        }

        public void Solve(ICluster? enter = null, ICluster? exit = null)
        {
            SolveData = new PathData
            {
                Enter = this,
                Exit = this,
                Path = new List<ICluster> {this}
            };
        }

        public void AddCluster(ICluster cluster)
        {
        }

        public void Reduce()
        {
        }

        public ICluster ClosestTo(ICluster cluster, ICluster? ignore = null) =>
            this;

        public double Distance() =>
            0.0;

        public List<ICluster> Path() =>
            new() {this};
    }
}