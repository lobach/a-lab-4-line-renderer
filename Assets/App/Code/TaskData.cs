﻿using System.Collections.Generic;
using UnityEngine;

namespace App.Code
{
    public class TaskData
    {
        public int Size => Points.Count;
        public List<Vector2Double> Points { get; set; }
    }
}