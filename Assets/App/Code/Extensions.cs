﻿using System;
using System.Collections.Generic;

namespace App.Code
{
    public static class Extensions
    {
        public static int ToInt(this string str) =>
            Convert.ToInt32(str);

        public static float ToFloat(this string str) =>
            Convert.ToSingle(str.Replace('.', ','));
        
        public static double ToDouble(this string str) =>
            Convert.ToDouble(str.Replace('.', ','));

        public static string ToStr<T>(this IList<T> lst)
        {
            var str = "[ ";
            for (var i = 0; i < lst.Count; i++)
            {
                if (i == lst.Count - 1)
                    str += $"{lst[i]} ]";
                else
                    str += $"{lst[i]}, ";
            }

            return str;
        }

        public static string ToStr<T>(this T[] lst)
        {
            var str = "[ ";
            for (var i = 0; i < lst.Length; i++)
            {
                if (i == lst.Length - 1)
                    str += $"{lst[i]} ]";
                else
                    str += $"{lst[i]}, ";
            }

            return str;
        }
    }
}